import { terser } from "rollup-plugin-terser";
import path from "path";
 
export default {
  input: path.resolve(__dirname, "javascript", "index.js"),
  output: {
    file: "build/bundle.js",
    format: 'iife'
  },
  plugins: [terser()]
};