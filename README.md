# Untitled Quiz Tool

This is a quiz tool to help megs study the animal anatomies.

Copy the `.env.example` to a file called `.env` and add a value for `SECRET`

To setup install node and then install yarn:
`npm i -g yarn`

Then run `yarn` to install dependencies.

Then run `yarn start` to start a server.

# Todos:
- Achievements