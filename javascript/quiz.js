export default () => {
    const image = document.querySelector('img');
    const answersList = document.querySelector('.answers-list');
    const resultSection = document.querySelector('.result-section');
    const nextButton = document.querySelector('.next');
    const progress = document.querySelector('progress');
    const circleSize = 18 / 2;
    const MAX_ITEMS = 4;
    const correctAnswers = [];

    const positionAnswer = (answer, callback = null) => {
        const imageBounding = image.getBoundingClientRect();
        const answerElement = document.querySelector(`div[data-id="${answer.id}"]`);

        answerElement.style.display = '';

        answerElement.style.left = ((answer.x * image.width) + imageBounding.left - circleSize + + document.documentElement.scrollLeft) + "px";
        answerElement.style.top = ((answer.y * image.height) + imageBounding.top - circleSize + document.documentElement.scrollTop) + "px";

        if (callback !== null) {
            answerElement.style.cursor = 'pointer';
            answerElement.onclick = callback;
        }
    };

    const renderAnswer = (answer, callback = null) => {
        answer.shouldReposition = true;
        positionAnswer(answer, callback);
    };

    const renderMessage = (result) => {
        resultSection.innerHTML = result;
    };

    const listName = (answer, callback) => {
        const button = document.createElement('button');
        button.innerText = answer.answer;
        button.className = 'button is-light';
        button.onclick = callback;
        answersList.appendChild(button);
    };

    const disableAnswerList = () => {
        for (const listItem of answersList.children)
        {
            listItem.disabled = true;
        }
    };

    const clearList = () => {
        answersList.innerHTML = '';
    }

    const hideAnswer = (answer) => {
        answer.shouldReposition = false;
        const answerElement = document.querySelector(`div[data-id="${answer.id}"]`);
        answerElement.style.display = 'none';
        answerElement.style.cursor = '';
        answerElement.onclick = () => {};
    };

    const hideAnswers = () => {
        for (const answer of answers) {
            hideAnswer(answer);
        }
    }

    const repositionAnswers = () => {
        for (const answer of answers) {
            if (answer.shouldReposition) {
                positionAnswer(answer);
            }
        }
    };

    const getRandomShuffle = (answer) => {
        const shuffled = [...answers]
            .sort(() => 0.5 - Math.random())
            .slice(0, MAX_ITEMS);

        if (shuffled.filter(({ id }) => id === answer.id).length === 0)
        {
            shuffled.pop();
            shuffled.push(answer);
        }

        return shuffled.sort(() => 0.5 - Math.random());
    };

    const giveNameQuiz = (answerIndex, callback) => {
        const answer = answers[answerIndex];
        renderAnswer(answer);

        renderMessage('Which part is this?');

        const selectedAnswers = getRandomShuffle(answer);

        for (const selectedAnswer of selectedAnswers) {
            listName(selectedAnswer, () => {
                disableAnswerList();
                callback(selectedAnswer.id === answer.id || selectedAnswer.answer === answer.answer);
            });
        }
    };

    const chooseItemQuiz = (answerIndex, callback) => {
        const answer = answers[answerIndex];
        renderMessage(`Choose the "${answer.answer}".`);

        const selectedAnswers = getRandomShuffle(answer);

        for (const selectedAnswer of selectedAnswers) {
            renderAnswer(selectedAnswer, () => {
                callback(selectedAnswer.id === answer.id || selectedAnswer.answer === answer.answer);
            });
        }
    };

    const showNextButton = () => {
        nextButton.style.display = '';
    };

    document.nextQuestion = () => {
        progress.value = correctAnswers.length;
        nextButton.style.display = 'none';
        clearList();
        renderMessage('');
        hideAnswers();
        if (correctAnswers.length < answers.length) {
            doQuiz();
        } else {
            renderMessage('<i class="fas fa-glass-cheers"></i> Well done!<br/>You finished the quiz!');
            document.querySelector('#go-back').style.display = '';
        }
    };

    const doQuiz = () => {
        let answerIndex = -1;
        do
        {
            answerIndex = Math.floor(Math.random() * answers.length);
        } while (correctAnswers.includes(answerIndex));
        
        if (Math.random() > 0.5) {
            giveNameQuiz(answerIndex, (isCorrect) => {
                renderMessage(isCorrect ? '✔️ Well done!' : `❌ The answer was:<br/>${answers[answerIndex].answer}`);
                showNextButton();
                repositionAnswers();
                if (isCorrect) {
                    correctAnswers.push(answerIndex);
                }
            });
        } else {
            chooseItemQuiz(answerIndex, (isCorrect) => {
                hideAnswers();
                renderAnswer(answers[answerIndex]);
                renderMessage(isCorrect ? '✔️ Well done!' : '❌ This was the answer.');
                showNextButton();
                repositionAnswers();
                if (isCorrect) {
                    correctAnswers.push(answerIndex);
                }
            });
        }

        repositionAnswers();
    };

    const start = () => {
        doQuiz();
        window.onresize = repositionAnswers;
        window.onscroll = repositionAnswers;
    };

    if (image.complete) {
        start();
    } else {
        image.addEventListener('load', start);
    }
};
