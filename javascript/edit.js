export default () => {
    const image = document.querySelector('img');
    const form = document.querySelector('form');
    const selection = document.querySelector('.selection');
    const answers = document.querySelectorAll('.answer');
    const selectionSize = 12 / 2;

    const start = () => {
        image.onclick = (event) => {
            const boundRect = image.getBoundingClientRect();

            const x = event.pageX - boundRect.left - document.documentElement.scrollLeft;
            const y = event.pageY - boundRect.top - document.documentElement.scrollTop;

            form.x.value = x / image.width;
            form.y.value = y / image.height;

            form.style.display = '';

            selection.style.display = 'inline-block';
            selection.style.left = (x - selectionSize + boundRect.left + document.documentElement.scrollLeft) + "px";
            selection.style.top = (y - selectionSize + boundRect.top + document.documentElement.scrollTop) + "px";
        };

        const repositionAnswers = () => {
            const boundRect = image.getBoundingClientRect();

            for (const answer of answers) {
                answer.style.display = '';
                const circle = answer.querySelector('.circle');

                answer.style.left = (answer.dataset.x * image.width + boundRect.left - circle.offsetLeft - selectionSize + document.documentElement.scrollLeft) + "px";
                answer.style.top = (answer.dataset.y * image.height + boundRect.top - circle.offsetTop - selectionSize + document.documentElement.scrollTop) + "px";
            }
        };

        window.onresize = repositionAnswers;
        window.onscroll = repositionAnswers;

        repositionAnswers();
    };

    if (image.complete) {
        start();
    }
    else {
        image.addEventListener('load', start);
    }
};
