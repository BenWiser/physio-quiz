export default () => {
    const tags = document.querySelectorAll('.tag');
    const tests = document.querySelectorAll('li[data-tags]');

    const selectedTags = {};

    for (const tag of tags) {
        tag.addEventListener('click', () => {
            if (tag.classList.contains('is-light')) {
                tag.classList.remove('is-light');
                tag.classList.add('is-primary');
                selectedTags[tag.innerHTML] = true;
            } else {
                tag.classList.add('is-light');
                tag.classList.remove('is-primary');
                delete selectedTags[tag.innerHTML];
            }

            const currentSelectedTags = Object.keys(selectedTags);

            for (const test of tests) {
                const testTags = Object.values(JSON.parse(test.dataset.tags)).map(({ tag }) => tag);

                let isVisible = true;
                for (const selectedTag of currentSelectedTags) {
                    if (!testTags.includes(selectedTag)) {
                        isVisible = false;
                        break;
                    }
                }

                test.style.display = isVisible ? '' : 'none';
            }
        });
    }
};