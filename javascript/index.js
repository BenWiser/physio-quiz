import edit from './edit';
import quiz from './quiz';
import quizHome from './quiz_home';

Turbolinks.setProgressBarDelay(200);

document.addEventListener('turbolinks:load', () => {
    // Remove hangine listeners pages might add
    window.onresize = () => {};
    window.onscroll = () => {};

    if (location.pathname.match("/edit/test/[0-9]+")) {
        edit();
    } else if (location.pathname.match("/quiz/[0-9]+")) {
        quiz();
    } else if (location.pathname.match("/quiz")) {
        quizHome();
    }
});