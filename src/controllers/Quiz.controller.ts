import { Controller, Get, PathParams, Request, UseBefore, View } from "@tsed/common";
import { Op } from "sequelize";
import { IsAuthenticated } from "../middlewares/IsAuthenticated.middleware";
import Account from "../models/Account.model";
import AnswerModel from "../models/Answer.model";
import Reference from "../models/Reference.model";
import StudyGroup from "../models/StudyGroup.model";
import StudyGroupAccount from "../models/StudyGroupAccount.model";
import Tag from "../models/Tag.model";
import Test from "../models/Test.model";
import TestModel from '../models/Test.model';

@Controller("/quiz")
@UseBefore(IsAuthenticated)
export class QuizController {
    @Get("/")
    @View("quiz_home.hbs")
    async getHome(@Request() { session: { accountId } }) {
        const LONG_TEST_AMOUNT = 10;

        const [account, tags] = await Promise.all([Account.findOne({
            where: {
                id: accountId,
            },
            include: [StudyGroup, {
                model: TestModel,
                include: [Account, AnswerModel, {
                    model: Tag,
                    attributes: ["tag"],
                }],
            }],
        }), Tag.findAll({
            include: [TestModel]
        })]);

        const studyGroupAccounts = await StudyGroupAccount.findAll({
            where: {
                studyGroupId: {
                    [Op.in]: account.studyGroups.map(({ id }) => id)
                },
            },
            include: [{
                model: Account,
                include: [{
                    model: Test,
                    include: [Account, AnswerModel, {
                        model: Tag,
                        attributes: ["tag"],
                    }],
                }],
            }],
        });


        const tests: { [id: number]: Test } = {};

        for (const test of account.tests) {
            tests[test.id] = test;
        }

        for (const studyGroupAccount of studyGroupAccounts) {
            for (const test of studyGroupAccount.account.tests) {
                tests[test.id] = test;
            }
        }

        return {
            long_tests: Object.values(tests).filter((test) => test.answers.length > LONG_TEST_AMOUNT)
                .map((test) => test.toJSON()),
            short_tests: Object.values(tests).filter((test) => test.answers.length <= LONG_TEST_AMOUNT)
                .map((test) => test.toJSON()),
            tags: tags
                .filter((tag) => tag.tests.length)
                .map((tag) => tag.toJSON()),
        };
    }

    @Get("/:id")
    @View("quiz.hbs")
    async getQuiz(@PathParams() { id } : { id: number }) {
        const test = await TestModel.findOne({
            where: {
                id,
            },
            include: [AnswerModel, Account, Reference],
        });
        return {
            test: test.toJSON(),
            account: test.account.toJSON(),
            answers: test.answers.map((answer) => answer.toJSON()),
            references: test.references.map((reference) => reference.toJSON()),
        };
    }
}