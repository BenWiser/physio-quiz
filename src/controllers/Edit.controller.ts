import { BodyParams, Controller, Get, MultipartFile, PathParams, PlatformMulterFile, Post, Redirect, Request, Response, UseBefore, View } from "@tsed/common";
import { IsAuthenticated } from "../middlewares/IsAuthenticated.middleware";
import Answer from "../models/Answer.model";
import AnswerModel from '../models/Answer.model';
import Reference from "../models/Reference.model";
import Tag from "../models/Tag.model";
import Test from "../models/Test.model";
import TestModel from '../models/Test.model';

@Controller("/edit")
@UseBefore(IsAuthenticated)
export class HomeController {
    @Get("/")
    @View("edit_home.hbs")
    async getEditHome(@Request() request) {
        const tests = await TestModel.findAll();
        return {
            tests: (tests as any)
                .filter((test) => test.accountId === request.session.accountId)
                .map((test) => test.dataValues),   
        };
    }

    @Get("/test")
    @View("edit_test_new.hbs")
    getTestNew() {
        return {};
    }

    @Get("/delete/:id")
    @Redirect("/edit")
    async deleteTest(
        @Request() request,
        @PathParams() { id } : { id: number }
    ) {
        if ((await TestModel.findOne({ where: { accountId: request.session.accountId } })
        ) === null) {
            return;
        }
        await AnswerModel.destroy({
            where: {
                testId: id,
            },
        });
        await TestModel.destroy({
            where: {
                id,
            },
        });
    }

    @Get("/answer/delete/:id")
    async deleteAnswer(
        @PathParams() { id } : { id: number },
        @Request() request,
        @Response() response) {
        const answer = await AnswerModel.findOne({
            where: {
                id,
            },
            include: [TestModel],
        });
        if (answer !== null && answer.test.accountId == request.session.accountId) {
            await AnswerModel.destroy({
                where: {
                    id,
                },
            });
            response.redirect(`/edit/test/${answer.testId}`)
        } else {
            response.redirect('/');
        }

        return response;
    }

    @Post("/test")
    async createTest(
        @BodyParams() { Name } : { Name: string },
        @Request() request,
        @MultipartFile("Image") image: PlatformMulterFile,
        @Response() response,
    ) {
        if (!image || !image.mimetype.startsWith("image")) {
            response.redirect('/edit/test');
        } else {
            const test = await TestModel.create({
                name: Name,
                image: image.filename,
                image_mime_type: image.mimetype,
                accountId: request.session.accountId,
            });
            response.redirect(`/edit/test/${test.id}`);
        }
        return response;
    }

    @Get("/test/:id")
    @View("edit_test.hbs")
    async getTest(@PathParams() { id }: { id: number }, @Response() response) {
        const test = await TestModel.findOne({
            where: {
                id,
            },
            include: [Answer, Reference, Tag],
        });
        if (test === null) {
            response.redirect("/edit");
            return response;
        }
        return {
            test: (test as any).dataValues,
            answers: (test.answers as any).map((answer) => answer.dataValues),
            references: (test.references as any).map((reference) => reference.dataValues),
            tags: (test.tags as any).map((tag) => tag.dataValues),
        };
    }

    @Post("/test/:id")
    async saveAnswer(
        @PathParams() { id } : { id: number },
        @BodyParams() { answer, x, y }:
        { answer: string, x: number, y: number },
        @Request() request,
        @Response() response,
    ) {
        const test = await Test.findOne({
            where: {
                id,
                accountId: request.session.accountId,
            },
        });

        if (test !== null) {
            await AnswerModel.create({
                x,
                y,
                answer,
                testId: id,
            });
        }
        response.redirect(`/edit/test/${id}`);
    }
}