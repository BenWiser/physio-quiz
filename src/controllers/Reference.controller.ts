import { BodyParams, Controller, Get, PathParams, Post, Request, Response, UseBefore } from "@tsed/common";
import { IsAuthenticated } from "../middlewares/IsAuthenticated.middleware";
import TestModel from '../models/Test.model';
import ReferenceModel from '../models/Reference.model';

@Controller("/reference")
@UseBefore(IsAuthenticated)
export class ReferenceController {
    @Post("/test/:id")
    async addReference(
        @Request() request,
        @PathParams() { id } : { id: number },
        @BodyParams() { reference }: { reference: string },
        @Response() response,
    ) {
        const test = await TestModel.findOne({
            where: {
                id,
            },
        });
        if (test !== null && request.session.accountId === test.accountId) {
            await ReferenceModel.create({
                reference,
                testId: id,
            });
        }
        response.redirect(`/edit/test/${id}`);
        return response;
    }

    @Get("/:id/delete")
    async removeReference(
        @Request() request,
        @PathParams() { id } : { id: number },
        @Response() response,
    ) {
        const reference = await ReferenceModel.findOne({
            where: {
                id
            },
            include: [TestModel],
        });
        if (request.session.accountId === reference.test.accountId) {
            await ReferenceModel.destroy({
                where: {
                    id,
                },
            });
        }
        response.redirect(`/edit/test/${reference.testId}`);
        return response;
    }
}