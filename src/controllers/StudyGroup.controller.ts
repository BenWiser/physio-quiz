import { BodyParams, Controller, Get, PathParams, Post, Redirect, Request, UseBefore } from "@tsed/common";
import { IsAuthenticated } from "../middlewares/IsAuthenticated.middleware";
import Account from "../models/Account.model";
import StudyGroup from "../models/StudyGroup.model";

@Controller("/study-group")
@UseBefore(IsAuthenticated)
export default class StudyGroupController {
    @Post("/")
    @Redirect("/profile")
    async createStudyGroup(
            @BodyParams() { name } : { name: string },
            @Request() { session: { accountId } },
    ) {
        if (await StudyGroup.findOne({
            where: {
                name: name.toLocaleLowerCase(),
            },
        }) !== null) {
            return;
        }

        const [studyGroup, account] = await Promise.all([StudyGroup.create({
            name: name.toLocaleLowerCase(),
        }), Account.findOne({
            where: {
                id: accountId,
            }
        })]);

        if (account !== null) {
            await account.addStudyGroup(studyGroup);
        }
    }

    @Get("/:id/user/delete")
    @Redirect("/profile")
    async removeUserToStudyGroup(
            @PathParams() { id }: { id: number },
            @Request() { session: { accountId } },
    ) {
        const [studyGroup, account] = await Promise.all([StudyGroup.findOne({
            where: {
                id,
            },
        }), Account.findOne({
            where: {
                id: accountId,
            }
        })]);

        await studyGroup.removeAccount(account);
    }

    @Post("/:id/user")
    @Redirect("/profile")
    async addUserToStudyGroup(
            @PathParams() { id }: { id: number },
            @BodyParams() { username }: { username: number },
            @Request() { session: { accountId } },
    ) {
        const [studyGroup, accountAdding, accountToAdd] = await Promise.all([StudyGroup.findOne({
            where: {
                id,
            },
            include: [Account]
        }), Account.findOne({
            where: {
                id: accountId,
            }
        }), Account.findOne({
            where: {
                username,
            }
        })]);

        if (accountToAdd !== null && studyGroup.accounts.filter((account) => account.id === accountAdding.id).length) {
            await accountToAdd.addStudyGroup(studyGroup);
        }
    }
}