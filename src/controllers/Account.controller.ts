import { BodyParams, Controller, Get, Post, QueryParams, Request, Response, UseBefore, View } from "@tsed/common";
import * as md5 from 'md5';
import { Op } from "sequelize";
import { IsAuthenticated } from "../middlewares/IsAuthenticated.middleware";
import Account from "../models/Account.model";
import AccountModel from '../models/Account.model';
import StudyGroup from "../models/StudyGroup.model";

@Controller("/")
export class AccountController {
    @Get("/login")
    @View("login.hbs")
    getLogin() {}

    @Get("/register")
    @View("register.hbs")
    getRegister() {}

    @Get("/sign-out")
    @View("register.hbs")
    signoutUser(@Request() request, @Response() response) {
        response.cookie('connect.sid', null);
        request.session.destroy();
        response.redirect("/");
        return response;
    }

    @Post("/login")
    async loginUser(@BodyParams() { username, password }: { username: string, password: string },
        @Request() request,
        @Response() response)
    {
        const account = await AccountModel.findOne({
            where: {
                username,
                password: md5(password),
            },
        });

        if (account !== null) {
            request.session.accountId = account.id;
            response.redirect(`/?message=${encodeURI("Welcome back!")}`);
        } else {
            response.redirect(`/login?error=${encodeURI("No account was found with this username and password")}`);
        }

        return response;
    }

    @Post("/register")
    async registerUser(@BodyParams() { username, password, confirm_password }:
        { username: string, password: string, confirm_password: string },
        @Request() request,
        @Response() response)
    {
        if (username.length === 0 || password.length === 0) {
            response.redirect(`/register?error=${"The username and password cannot be empty"}`);
            return response;
        }

        if (password !== confirm_password) {
            response.redirect(`/register?error=${"The password did not match with the password confirmation"}`);
            return response;
        }

        const existingAccount = await AccountModel.findOne({
            where: {
                username,
            },
        });

        if (existingAccount !== null) {
            response.redirect(`/register?error=${encodeURI("This username is already in use")}`);
            return response;
        }

        const account = await AccountModel.create({
            username,
            password: md5(password),
        });

        request.session.accountId = account.id;

        response.redirect("/");
        return response;
    }

    @UseBefore(IsAuthenticated)
    @Get("/profile")
    @View("profile.hbs")
    async getProfile(@Request() request) {
        const account = await AccountModel.findOne({
            where: {
                id: request.session.accountId,
            },
            include: [{
                model: StudyGroup,
                include: [Account],
            }],
        });

        return {
            account: account.toJSON(),
        };
    }

    @UseBefore(IsAuthenticated)
    @Get("/account/search")
    async searchUsers(@QueryParams() { username }: { username: string }) {
        const accounts = await Account.findAll({
            where: {
                username: {
                    [Op.like]: `%${username}%`,
                },
            },
        });

        return {
            accounts,
        };
    }
}