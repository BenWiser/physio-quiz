import { Controller, Get, Request, View } from "@tsed/common";
import * as greetingTime from 'greeting-time';
import AccountModel from '../models/Account.model';

@Controller("/")
export class HomeController {
    @Get("/")
    @View("home.hbs")
    async getHome(@Request() request) {
        const { accountId } = request.session;
        return {
            accountId,
            account: (accountId ? (await AccountModel.findOne({
                where: {
                    id: accountId,
                },
            })as any).dataValues : undefined),
            greeting: greetingTime(new Date()),
        };
    }

    @Get("/terms")
    @View("terms.hbs")
    getTerms() {}
}