import { BodyParams, Controller, Get, PathParams, Post, Request, Response, UseBefore } from "@tsed/common";
import { IsAuthenticated } from "../middlewares/IsAuthenticated.middleware";
import TestModel from '../models/Test.model';
import Tag from "../models/Tag.model";

@Controller("/tags")
@UseBefore(IsAuthenticated)
export class TagController {
    @Post("/test/:id")
    async addTag(
        @Request() request,
        @PathParams() { id } : { id: number },
        @BodyParams() { tag }: { tag: string },
        @Response() response,
    ) {
        const test = await TestModel.findOne({
            where: {
                id,
            },
        });
        if (test !== null && request.session.accountId === test.accountId) {
            await test.addTag((await Tag.findOrCreate({
                where: {
                    tag: tag.toLocaleLowerCase().replace(' ', '-'),
                },
            }))[0]);
        }
        response.redirect(`/edit/test/${id}`);
        return response;
    }

    @Get("/:testId/:tagId/delete")
    async removeReference(
        @PathParams() { testId, tagId } : { testId: number, tagId: number },
        @Response() response,
    ) {
        const [tag, test] = await Promise.all([Tag.findOne({
            where: {
                id: tagId
            },
            include: [TestModel],
        }), TestModel.findOne({
            where: {
                id: testId,
            },
        })]);
        if (tag !== null) {
            await test.removeTag(tag);
        }
        response.redirect(`/edit/test/${testId}`);
        return response;
    }
}