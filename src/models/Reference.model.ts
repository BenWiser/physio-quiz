import { Table, Model, Column, BelongsTo, ForeignKey } from 'sequelize-typescript';
import TestModel from './Test.model';

@Table
export default class Reference extends Model {
    @Column
    reference: string;

    @ForeignKey(() => TestModel)
    @Column
    testId: number;

    @BelongsTo(() => TestModel)
    test: TestModel;
}