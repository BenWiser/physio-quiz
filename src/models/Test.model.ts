import { Table, Model, Column, HasMany, BelongsTo, ForeignKey, BelongsToMany } from 'sequelize-typescript';
import Answer from './Answer.model';
import Account from './Account.model';
import ReferenceModel from './Reference.model';
import Tag from './Tag.model';
import TestTag from './TestTag.model';

@Table
export default class Test extends Model {
    @Column
    name: string;

    @Column
    image: string;

    @Column
    image_mime_type: string;

    @HasMany(() => Answer)
    answers: Answer[];

    @HasMany(() => ReferenceModel)
    references: ReferenceModel[];

    @ForeignKey(() => Account)
    @Column
    accountId: number;

    @BelongsTo(() => Account)
    account: Account;

    @BelongsToMany(() => Tag, () => TestTag)
    tags: Tag[];

    addTag: (tag: Tag) => Promise<any>;

    removeTag: (tag: Tag) => Promise<any>;
}