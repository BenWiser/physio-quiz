import { BelongsToMany, Column, HasMany, Model, Table } from 'sequelize-typescript';
import StudyGroup from './StudyGroup.model';
import StudyGroupAccount from './StudyGroupAccount.model';
import Test from './Test.model';

@Table
class Account extends Model {
    @Column
    username: string;

    @Column
    password: string;

    @HasMany(() => Test)
    tests: Test[];

    @BelongsToMany(() => StudyGroup, () => StudyGroupAccount)
    studyGroups: StudyGroup[];

    addStudyGroup: (studyGroup: StudyGroup) => Promise<any>;

    removeStudyGroup: (studyGroup: StudyGroup) => Promise<any>;
}

export default Account;