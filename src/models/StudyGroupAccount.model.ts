import { ForeignKey, BelongsTo, Model, Table } from 'sequelize-typescript';
import Account from './Account.model';
import StudyGroup from './StudyGroup.model';

@Table
export default class StudyGroupAccount extends Model {
    @ForeignKey(() => Account)
    accountId: number;

    @ForeignKey(() => StudyGroup)
    studyGroupId: number;

    @BelongsTo(() => Account)
    account: Account;

    @BelongsTo(() => StudyGroup)
    studyGroup: StudyGroup;
}