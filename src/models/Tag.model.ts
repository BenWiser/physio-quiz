import { BelongsToMany, Column, Model, Table } from 'sequelize-typescript';
import Test from './Test.model';
import TestTag from './TestTag.model';

@Table
export default class Tag extends Model {
    @Column
    tag: string;

    @BelongsToMany(() => Test, () => TestTag)
    tests: Test[];
}