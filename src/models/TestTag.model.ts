import { ForeignKey, Model, Table } from 'sequelize-typescript';
import Tag from './Tag.model';
import Test from './Test.model';

@Table
export default class TestTag extends Model {
    @ForeignKey(() => Tag)
    tagId: number;

    @ForeignKey(() => Test)
    testId: number;
}