import { BelongsToMany, Column, Model, Table } from 'sequelize-typescript';
import Account from './Account.model';
import StudyGroupAccount from './StudyGroupAccount.model';

@Table
export default class StudyGroup extends Model {
    @Column
    name: string;

    @BelongsToMany(() => Account, () => StudyGroupAccount)
    accounts: Account[];

    addAccount: (account: Account) => Promise<any>;

    removeAccount: (account: Account) => Promise<any>;
}