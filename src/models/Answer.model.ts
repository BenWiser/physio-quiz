import { BelongsTo, Column, ForeignKey, Model, Table } from 'sequelize-typescript';
import TestModel from './Test.model';

@Table
class Answer extends Model {
    @Column
    x: number;

    @Column
    y: number;

    @Column
    answer: string;

    @BelongsTo(() => TestModel)
    test: TestModel;

    @ForeignKey(() => TestModel)
    @Column
    testId: number;
}

export default Answer;