import { Middleware, Next, QueryParams, Request, Response, Session } from '@tsed/common';
import * as handlebars from 'handlebars';
import * as Path from 'path';
import * as fs from 'fs';

@Middleware()
export default class AddLayouts {
    async use(
        @QueryParams() { error, message },
        @Response() response,
        @Request() request,
        @Next() next,
    ) {
        const layoutsPath = Path.resolve(__dirname, '..', '..', 'views', 'layout');

        const getLayout = (layout) => new Promise((resolve) => {
            fs.readFile(Path.resolve(layoutsPath, layout), (err, data) => {
                const template = handlebars.compile(data.toString());
                resolve(template({
                    error,
                    message,
                    accountId: request.session.accountId,
                }));
            })
        });
        const [header, notifications, footer] = await Promise.all([
            getLayout("header.hbs"),
            getLayout("notifications.hbs"),
            getLayout("footer.hbs"),
        ]);

        response.locals.header = header;
        response.locals.notifications = notifications;
        response.locals.footer = footer;

        next();
    }
}