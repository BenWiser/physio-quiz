import {Middleware, Next, Request, Response} from "@tsed/common";

@Middleware()
export class IsAuthenticated {
  use(@Request() request, @Response() response, @Next() next) {
    if (request.session.accountId) {
        next();
    } else {
        response.redirect("/");
        return response;
    }
  }
}