import { Sequelize } from 'sequelize-typescript';
const database = new Sequelize({
    database: 'db',
    dialect: 'sqlite',
    storage: 'db.db',
    models: [__dirname + '/models/**/*.model.ts'],
});

export default database;