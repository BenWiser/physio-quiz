import {Configuration, Inject} from "@tsed/di";
import {PlatformApplication} from "@tsed/common";
import "@tsed/platform-express";
import * as Path from "path";
import database from './database';  
import AddLayouts from './middlewares/AddLayouts.middleware';

const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const compress = require('compression');
const methodOverride = require('method-override');
const session = require('express-session');
const expressSession = require('express-session');
const SessionStore = require('express-session-sequelize')(expressSession.Store);

const rootDir = Path.resolve(__dirname);

const sequelizeSessionStore = new SessionStore({
  db: database,
});

@Configuration({
  rootDir,
  mount: {
      "/": [Path.resolve(rootDir, "controllers", "**", "*.controller.ts")]
  },
  componentsScan: [
    Path.resolve(rootDir, 'middlewares', '**', '*.middleware.ts'),
  ],
  port: 3000,
  viewsDir: `${rootDir}/views`,
  multer: {
    dest: `${rootDir}/../uploads`,
  },
  statics: {
    "/": [
      {
        root: `${rootDir}/../static`,
      },
    ],
    "/css": [
      {
        root: `${rootDir}/../css`,
      }
    ],
    "/images": [
      {
        root: `${rootDir}/../uploads`,
      },
      {
        root: `${rootDir}/../images`,
      }
    ],
    "/javascript": [
      {
        root: `${rootDir}/../javascript`,
      },
      {
        root: `${rootDir}/../build`
      }
    ]
  },
  views: {
    root: `${rootDir}/views`,
    viewEngine: "handlebars",
    extensions: {
        "hbs": "handlebars"
    },
    options: {
        handlebars: {
            helpers: {
                toJSON: function(object) {
                    return JSON.stringify(object);
                },
                isDefined: function(value) {
                  return value !== undefined;
                },
                isEmpty: function(list) {
                  return list.length === 0;
                },
                not: function(value) {
                  return !value;
                },
                eq(value1, value2) {
                  return value1 === value2;
                },
                and(value1, value2) {
                  return value1 && value2;
                },
                concat(...values) {
                  return values.splice(0, values.length - 1).join("");
                },
                array(...values) {
                  console.log("TEST TEST ", values);
                  return values.splice(0, values.length - 1);
                },
                breadcrumbs (...links) {
                  let result = `
                  <nav class="breadcrumb" aria-label="breadcrumbs">
                  <ul>`;
                  for (let i = 0; i < links.length - 1; i++) {
                    const link = links[i];
                    if (i === links.length - 2) {
                      result += `<li class="is-active"><a href="${link[0]}" aria-current="page">${link[1]}</a></li>`;
                    } else {
                      result += `<li><a href="${link[0]}">${link[1]}</a></li>`;
                    }
                  }
                  result += `</ul>
                  </nav>`;
                  return result;
                },
            },
        }
    }
  }
})
export class Server {
  @Inject()
  public app: PlatformApplication;

  public $beforeRoutesInit() {
    this.app
      .use(cookieParser())
      .use(compress({}))
      .use(methodOverride())
      .use(bodyParser.json())
      .use(bodyParser.urlencoded({
        extended: true
      }))
      .use(session({
        store: sequelizeSessionStore,
        secret : process.env.SECRET,
        resave : false,
        saveUninitialized: false,
        cookie: {
          expires: new Date(Date.now() + 60 * 10000), 
          maxAge: 60*10000,
        },
      }))
      .use(AddLayouts);
  }   
}