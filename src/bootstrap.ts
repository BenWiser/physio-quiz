import { $log } from "@tsed/common";
import { PlatformExpress } from "@tsed/platform-express";
import { Server } from "./Server";
import database from './database';
require('dotenv').config();

async function bootstrap() {
    await database.sync();

    try {
        $log.debug("Start server...");
        const platform = await PlatformExpress.bootstrap(Server);

        await platform.listen();
        $log.debug("Server initialized");
    } catch (er) {
        $log.error(er);
    }
}

bootstrap();